module.exports = {
     getError(errors, prop){
        //prop = "email" || "password" 
       try{
            return errors.mapped()[prop].msg
    
           /* The return statement above will give errors.mapped()==={
                email:{msg: 'Invalid email'},
            } */
        } catch (err){
            return '';
        }
    }
};