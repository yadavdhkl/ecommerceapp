const express = require("express");
const cartsRepo = require('../repositories/cart');

const router = express.Router();

//Post request to add item in cart
router.post('/cart/products', async (req, res) =>{
    //console.log(req.body.productId);
    //Figure out the cart
    let cart;
    if(!req.session.cartId){
        //We dont have cart and have to create and store cart id in req.session.cartId
        cart = await cartsRepo.create({ items: []})
        req.session.cartId = cart.id;

    }else {
        //We have cart and let get it from the repository
        cart = await cartsRepo.getOne(req.session.cartId);
    }
    //console.log(cart);
    const existingItem = cart.items.find(item=> item.id === req.body.productId);
    if(existingItem){
        //Add quantity
        existingItem.quantity++;
    }else{
        //Add new item
        cart.items.push({id: req.body.productId, quantity:1});
    }
    //Either increment quantity for existing item
    //OR add ned item to an array
    await cartsRepo.update(cart.id, {items: cart.items})
    res.send('Product added to cart')
})
//Get to display all item in the cart
//Delete item from the cart

module.exports = router;