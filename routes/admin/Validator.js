const { check } = require('express-validator');
const userRepo = require('../../repositories/users');

module.exports = {
    requireEmail: check('email').trim().normalizeEmail().isEmail().withMessage('Must be valid email')
        .custom(async email =>{
            const existingUser = await userRepo.getOneBy({email});
            if(existingUser){
                return res.send('Email already exist!!');
            }
        }),
    requirePassword: check('password').trim().isLength({ min: 4, max: 20 }).withMessage('Password must be between 4 to 20 character'),
    requirepasswordConfirmation: check('passwordConfirmation').trim().isLength({ min: 4, max: 20 })
        .withMessage('Password must be between 4 to20 character')
        .custom((passwordConfirmation, {req}) =>{
            if(passwordConfirmation !== req.body.password){
                throw new Error('Password must match!!');
            }
            return true;
        }),
    requireValidEmail:  check('email').trim().normalizeEmail().isEmail().withMessage('Must provide a valid email')
        .custom(async email => {
            const user = await userRepo.getOneBy({email});

            if(!user){
                return new Error('Email not found!!');
            }
        }),
    requireValidPasswordforUser: check('password').trim().custom(async (password, {req}) =>{
        const user = await userRepo.getOneBy({ email: req.body.email });
        if(!user){
            return new Error('Invalid password!');
        }
        const validPassword = await userRepo.comparePassword(user.password, password);

        if(!validPassword){
            return new Error('Invalid password')
        }
    }),
    requireTitle: check("title").trim().isLength({ min: 5, max: 40 }).withMessage("Must be between 5 and 40 characters"),
    requirePrice: check("price").trim().toFloat().isFloat({ min: 1 }).withMessage("Must be a number greater than 1")
}