const express = require('express');

const { handleErrors } = require('./middlewares');
const userRepo = require('../../repositories/users');
const signupTemplate = require('../../views/admin/auth/signup');
const signinTemplate = require('../../views/admin/auth/signin');
const { check, validationResult } = require('express-validator');
const { requireEmail, requirePassword, requirepasswordConfirmation, requireValidEmail, requireValidPasswordforUser } = require('./Validator')

const router = express.Router()

//route handler
router.get('/signup', (req, res) =>{
    res.send(signupTemplate({ req }));
});

router.post('/signup', [ requireEmail, requirePassword, requirepasswordConfirmation], handleErrors(signupTemplate), async (req, res)=> {
    //get access to email, password, passwordconformation
    const {email, password, passwordConfirmation} = req.body;

   // Create a user in our user repo to represent that user
   const user = await userRepo.create({email, password});

   // Store the cookie of that user inside the user cookie
   req.session.userId = user.id; // Added by cookie session!

   //res.send("Account created");
   res.redirect("/admin/products");
})

router.get('/signout', (req, res) => {
    req.session = null;
    res.send('You are sign out!!!')
});

router.get('/signin', (req, res) => {
    res.send(signinTemplate({}))
});

router.post('/signin', [requireValidEmail, requireValidPasswordforUser ], handleErrors(signinTemplate), async (req, res) => {
   
    const {email} = req.body; 

    const user = await userRepo.getOneBy({email});
   
    req.session.userId = user.id;
   // res.send('You are signed in...')
   res.redirect("/admin/products");
});

module.exports = router;
