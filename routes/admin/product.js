const express = require("express");
var multer  = require('multer');

const { handleErrors, requireAuth } = require('./middlewares');
const router = express.Router();
const productRepo = require('../../repositories/products');
const productsNewTemplate = require('../../views/admin/products/new');
const productsIndexTemplate = require('../../views/admin/products/index');
const productEditTemplate = require('../../views/admin/products/edit');
const { requireTitle, requirePrice } = require('./Validator');

var upload = multer({ storage: multer.memoryStorage() });

router.get("/admin/products", requireAuth, async (req, res) => {

    const products = await productRepo.getAll();
    res.send(productsIndexTemplate({ products }));
});

router.get("/admin/products/new", (req, res) => {
    res.send(productsNewTemplate({}));
});

router.post("/admin/products/new", requireAuth, upload.single('image'), [requireTitle, requirePrice], handleErrors(productsNewTemplate), 
async (req, res) => {  
    const image = req.file.buffer.toString('base64');
    const { title, price} = req.body;

    await productRepo.create({title, price, image});

    //res.send('Submitted..')
    res.redirect("/admin/products");
});

router.get('/admin/products/:id/edit', requireAuth, async(req, res) => {
    //console.log(req.params.id);
    const product = await productRepo.getOne(req.params.id);
    if(!product){
        res.send('Product not found!!');
    }
    res.send(productEditTemplate({ product }));
});

router.post("/admin/products/:id/edit", requireAuth, upload.single("image"), [requireTitle, requirePrice],
    handleErrors(productEditTemplate, async (req) => {
        const product = await productRepo.getOne(req.params.id);
        return { product };
    }),
    async (req, res) => {
        const changes = req.body;
        if (req.file) {
            changes.image = req.file.buffer.toString("base64");
        }
        try {
            await productRepo.update(req.params.id, changes);
        } catch (err) {
            return res.send("Could not find item");
        }
        res.redirect("/admin/products");
    }
);

router.post("/admin/products/:id/delete", requireAuth, async(req, res) => {
   await productRepo.delete(req.params.id);
   res.redirect("/admin/products");
});

module.exports = router;