const express = require('express');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const authRouter = require('./routes/admin/auth');
const adminproductRouter = require('./routes/admin/product');
const productRouter = require('./routes/products');
const cartRouter = require('./routes/carts');

const app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieSession({
    keys: ['sbvdksbcsksjcjsdhcbsd']
}));
app.use(authRouter);
app.use(adminproductRouter);
app.use(productRouter);
app.use(cartRouter);

app.listen(3000, ()=>{
    console.log('listenting on this port');
})

